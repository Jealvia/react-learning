import React from 'react';


export default class TaskForm extends React.Component{
    
    state={
        title:'',
        descripcion:''
    }

    registrar=e=>{
        this.props.addTask(this.state.title,this.state.descripcion);
        e.preventDefault();
    }

    cambioInputs=e=>{
        this.setState({
            [e.target.name]:e.target.value,
            
        })
    }
    
    render(){
        return(
            <div>
                <form onSubmit={this.registrar}>
                    <input 
                    name="title"
                    type="text" 
                    placeholder="Escribe una tarea" onChange={this.cambioInputs} value={this.state.title}/>
                    <br />
                    <br />
                    <textarea 
                    name="descripcion"
                    placeholder="Escibe una descripcion" onChange={this.cambioInputs} value={this.state.descripcion}></textarea>
                    <br />
                    <br />
                    <button type="submit">Guardar</button>
                </form>
            </div>
        )
    }
}