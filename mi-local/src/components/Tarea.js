import React from 'react';
import PropTypes from 'prop-types';


class Tarea extends React.Component{

    estilos(){
        return{
            fontSize:'20px',
            color: this.props.task.done?'gray':'black'
        }
    }

    render(){
        const {task}=this.props;
        return <p style={this.estilos()}>
            {task.title}-{task.descripcion}-{task.id}
            <input type="checkbox" onChange={this.props.cambiando.bind(this,task.id)} />
            <button style={btnDelete} onClick={this.props.eliminar.bind(this,task.id)} >
                X
            </button>
        </p>
    }

    
}

//Ayudan a especificar que tipos de elementos reciben en los props
Tarea.propTypes={
    task: PropTypes.object.isRequired
}

const btnDelete={
        fontSize: '18px',
        background:'#ea2027',
        color:'#fff',
        border:'none',
        padding:'10px 15px',
        borderRadius:'50%',
        cursor:'pointer',
        //float:'right'
}

export default Tarea;