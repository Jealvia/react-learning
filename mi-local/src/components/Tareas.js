import React from 'react'
import Tarea from './Tarea';
class Tareas extends React.Component{

    render(){
        return this.props.tasks.map(e=> 
                  <Tarea 
                    key={e.id} 
                    task={e} 
                    eliminar={this.props.delete} 
                    cambiando={this.props.cambiar} 
                  />)
        
    }

}

export default Tareas;