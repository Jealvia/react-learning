import React from 'react';
//import logo from './logo.svg';
import './App.css';
//Datos de tareas
import tasks from './samples/tasks.json';
//Componentes
import Tareas from './components/Tareas';
import TaskForms from './components/TaskForm';
import Post from './components/Post'
//Routers
import {BrowserRouter as Router, Route,Link} from 'react-router-dom'


class App extends React.Component{

  state={
    tareas:tasks
  }

  agregarTarea=(title,descripcion)=>{
    const newTask={
      title:title,
      descripcion:descripcion,
      id:this.state.tareas.length
    }
    this.setState({
      tareas:[...this.state.tareas,newTask]
    })
    
  }
  
  eliminarTarea=id=>{
    const nuevasTareas=this.state.tareas.filter(task=>task.id!==id)
    this.setState({
      tareas:nuevasTareas
    })
  }

  changeDone=id=>{
    const nuevasTareas=this.state.tareas.map(task=>{
      if(task.id===id){
        task.done=!task.done
      }
      return task;
    })
      this.setState({
        tareas:nuevasTareas
      })
  }

  render(){
    return(
      <div>
        <Router>
          <Link to="/" >Inicio </Link>
          <br/>
          <Link to="/post" >Posts </Link>
          <Route exact path="/"  render={()=>{
            return <div>
              <TaskForms addTask={this.agregarTarea}/>
              <Tareas tasks={this.state.tareas} delete={this.eliminarTarea} cambiar={this.changeDone } />
              
            </div>
          }}>
          </Route>
          <Route path="/post" component={Post} />
        </Router>
        
        
      </div>
    )
  }

}


export default App;
